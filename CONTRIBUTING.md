# Contributing to this repo

Due to the nature of this repository, it doesn't suite itself to nature
contributions but if you found an issue with-in one of my slides feel free to
open an issue or a merge request for it. Please not that for some of these slides
this repo is not the master source, as they live in Google Slides, but issues
reported here will be corrected there and the change propigated into this repo.

## Reporting an issue

If you would like to report an issue please make sure to include the name of the
talk and the slide/page number to help me find it.

## Attribution

If you would like to have attribution for your contribution I would love to give
it to you. I will gladdly add your name to a `CONTRIBUTORS` list, if you would
rather not have your name listed that is fine as well I'll just need to know.