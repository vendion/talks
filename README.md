# Talks

Collection of talks given by vendion for various different user groups/events.

## Notes on slides

Some of the slides, the files that end with `.slide` requires the
[Go present](https://godoc.org/golang.org/x/tools/cmd/present) tool to be able
to render and view the slides.

To use the Go present tool make sure you have a recent version of Go installed,
if you don't see the [official install documentation](https://golang.org/doc/install)

With Go installed the present tool can be installed with the following

    $ go get -u golang.org/x/tools/cmd/present

With the present tool installed just `cd` into this repo and run the `present`
command

    $ present

Certain talks may require different flags to be passed to the present tool for
things to work correctly, like speaker notes `-notes` or running code in the
browser `-play`

## Licence

Copyright © 2021 Adam Jimerson <vendion@gmail.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the LICENSE file for more details.
