#!/usr/bin/env bash

some_func() {
  local arg1=$1
  local arg2=$2

  echo "I got ${arg1} and ${arg2} from my caller"
}

some_func2() {
  local arg1=$1
  local arg2=$2

  if [[ -z "${arg1}" || -z "${arg2}" ]]; then
    return 1
  fi

  return 0
}

# START OMIT
some_func "foo", "bar"
# END OMIT

# CAPSTART OMIT
output="$(some_func "foo", "bar")"
# CAPEND OMIT

# RETURNSTART OMIT
if ! some_func2 "foo" ; then
  echo "some_func2 failed: argument mismatch"
fi
# RETURNEND OMIT
