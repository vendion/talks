<!-- fg=white bg=black -->

# Declarative and Reproducible Systems with Nix

---

<!-- fg=white bg=black -->

## Who am I?

![codio](whoami.yml)

---

<!-- fg=white bg=black -->

## The problem (e.g. "It works on my machine")...

- Team members with differing environments.
  - Differing operating systems.
  - Differing CPU architectures.
  - Etc...
- Development environment differs than than of production.
- Need a way to easily onboard new team members.
- Need to standardize tools used for development.

---

<!-- effect=fireworks -->

## The solution...

![NixOS](images/nixos.256x223.png)

---

<!-- fg=white bg=black -->

## What is Nix?

- Nix is a Functional Programming Language.
- Nix is a cross-platform package manager.
- Nix is a Linux distribution.

---

<!-- fg=white bg=black -->

## Nix as an Operating System

NixOS is the grandfather of modern immutable Linux distributions.

- Each rebuild of the system generates a new boot environment.
- This allows for easy recovery from errors.
- Also allows for rolling the whole system back to an older state.
- Embodyment of infastructure as code.

---

<!-- fg=white bg=black -->

## What about Linux containers?

- Containers can help ensure a common environment.
- Can speed up the process of setting up development environments.

---

<!-- fg=white bg=black -->

## What about Linux containers?

- Containers can help ensure a common environment.
- Can speed up the process of setting up development environments.
- Are not reproducible.

---

<!-- fg=white bg=black -->

## How does Nix ensure reproducibility?

- Nix is a collection of inputs and outputs.
- Inputs define where nix should pull declarations from.
  - In general distro terms, these are like software repositories.
  - Nix tracks the commit hash, as well as a checksum for each input.
  - Inputs go beyond just supplying installable packages:
    - Disko - Nix module to declarativly partition drives (NixOS).
    - Hardware - Community maintained hardware profiles for (NixOS).
    - Stylix - Declarative ricing.
    - Nixvim - Neovim distribution built and managed using Nix.
    - home-manager - Declaratively handles user files.
    - sops - Declarative secrets management.
    - etc...
- Outputs define the system to be built.

My personal configuration: https://git.sr.ht/~vendion/vendionOS

---

<!-- fg=white bg=black -->

## Nix flakes

- Flakes make it easy to share configurations between users and machines.
- Exist of a "flake.nix" and "flake.lock".
- Works similarly to composer.json/composer.lock and package.json/package.lock files.
- Flakes can be monolithic or modular.

---

<!-- fg=white bg=black -->

## Installing packages (Option 1)

```nix
{
    environment.packages = [
        pkgs.firefox
        pkgs.git
        pkgs.neovim
    ];
}
```

---

<!-- fg=white bg=black -->

## Installing packages (Option 2)

```nix
{
    services.openssh = {
        enable = true;
        ports = [ 22 ];
        settings = {
            PasswordAuthentication = false;
            PermitRootLogin = "no";
        };
    };
}
```

---

<!-- fg=white bg=black -->

## Modifying packages

```nix
{
    fonts = {
          monospace = {
            name = "GoMono Nerd Font";
            package = pkgs.nerdfonts.override { fonts = [ "Go-Mono" ]; };
          };
          serif = {
            name = "Noto Serif";
            package = pkgs.noto-fonts;
          };
          sansSerif = {
            name = "Noto Sans";
            package = pkgs.noto-fonts;
          };
    };
}
```

---

<!-- fg=white bg=black -->

## Imperative shells

Imperative shells are temporary shell with its own set of packages and configurations.

Can be mixed with programs like `devenv` to give isolation to projects,
abstraction for common languages and services, and managing pre-commit hooks for
VCS systems and more.

---

<!-- fg=white bg=black -->

## Live Demo

---

<!-- effect=plasma -->

---

<!-- fg=white bg=black -->

## Thanks
